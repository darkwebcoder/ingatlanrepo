<?php

/* ::login.html.twig */
class __TwigTemplate_9e137f0f3f6b84163ea2799559d0f7a00b822e407fa0362e4baa72f674ea9407 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "::login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c03bb22323639958ed2cfa2e534569b3d6a28bd4402cff532ecbf0781ec2ced5 = $this->env->getExtension("native_profiler");
        $__internal_c03bb22323639958ed2cfa2e534569b3d6a28bd4402cff532ecbf0781ec2ced5->enter($__internal_c03bb22323639958ed2cfa2e534569b3d6a28bd4402cff532ecbf0781ec2ced5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c03bb22323639958ed2cfa2e534569b3d6a28bd4402cff532ecbf0781ec2ced5->leave($__internal_c03bb22323639958ed2cfa2e534569b3d6a28bd4402cff532ecbf0781ec2ced5_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_7ea6664f8f99c87ce2f938beb2a354b3454d5dac49b922d6066f89620ecf18d0 = $this->env->getExtension("native_profiler");
        $__internal_7ea6664f8f99c87ce2f938beb2a354b3454d5dac49b922d6066f89620ecf18d0->enter($__internal_7ea6664f8f99c87ce2f938beb2a354b3454d5dac49b922d6066f89620ecf18d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "    ";
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 6
            echo "        <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
    ";
        }
        // line 8
        echo "

        <div class=\"col-md-12 col-xs-12 whitebox\">
        <h2>Log in</h2>

        <div class=\"col-md-6\">
            <h4>If you are a registered user, please log in</h4>
            <form class=\"form-horizontal\" action=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
                <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label\" for=\"username\">Felhasználónév</label>
                    <div class=\"col-sm-8\">
                        <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                    </div>
                </div>
                <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label\" for=\"password\">Jelszó</label>
                    <div class=\"col-sm-8\">
                        <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" />
                    </div>
                </div>
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-4 col-sm-8\">
                        <div class=\"checkbox\">
                            <label>
                                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                                Emlékezz rám
                            </label>
                        </div>
                    </div>
                </div>
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-4 col-sm-8\">
                        <input type=\"submit\" id=\"_submit\" class=\"btn-primary btn\" name=\"_submit\" value=\"Belépés\" />
                    </div>
                </div>

            </form>
            <div class=\"fb-login-button\" data-max-rows=\"10\" data-size=\"medium\" data-show-faces=\"true\" data-auto-logout-link=\"false\"></div>

        </div>

        <div class=\"col-md-6\">
            <h4>If you are new around here, register</h4>

            <a href=\"\" class=\"btn btn-default\">Register</a>


        </div>


    </div>

";
        
        $__internal_7ea6664f8f99c87ce2f938beb2a354b3454d5dac49b922d6066f89620ecf18d0->leave($__internal_7ea6664f8f99c87ce2f938beb2a354b3454d5dac49b922d6066f89620ecf18d0_prof);

    }

    public function getTemplateName()
    {
        return "::login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 20,  62 => 16,  58 => 15,  49 => 8,  43 => 6,  40 => 5,  34 => 4,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"base.html.twig\" %}
{% trans_default_domain 'FOSUserBundle' %}

{% block body %}
    {% if error %}
        <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}


        <div class=\"col-md-12 col-xs-12 whitebox\">
        <h2>Log in</h2>

        <div class=\"col-md-6\">
            <h4>If you are a registered user, please log in</h4>
            <form class=\"form-horizontal\" action=\"{{  path(\"fos_user_security_check\")}}\" method=\"post\">
                <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
                <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label\" for=\"username\">Felhasználónév</label>
                    <div class=\"col-sm-8\">
                        <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />
                    </div>
                </div>
                <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label\" for=\"password\">Jelszó</label>
                    <div class=\"col-sm-8\">
                        <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" class=\"form-control\" />
                    </div>
                </div>
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-4 col-sm-8\">
                        <div class=\"checkbox\">
                            <label>
                                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                                Emlékezz rám
                            </label>
                        </div>
                    </div>
                </div>
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-4 col-sm-8\">
                        <input type=\"submit\" id=\"_submit\" class=\"btn-primary btn\" name=\"_submit\" value=\"Belépés\" />
                    </div>
                </div>

            </form>
            <div class=\"fb-login-button\" data-max-rows=\"10\" data-size=\"medium\" data-show-faces=\"true\" data-auto-logout-link=\"false\"></div>

        </div>

        <div class=\"col-md-6\">
            <h4>If you are new around here, register</h4>

            <a href=\"\" class=\"btn btn-default\">Register</a>


        </div>


    </div>

{% endblock body %}
";
    }
}
