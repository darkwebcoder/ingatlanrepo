<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_cb0cbbaa810546c87595b2a2d15ff41636eb2c94761d79311653e05b37a52cce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3616bf6c58414df2b2d9a42e3f35945657c4c9e82bc0adfc6fdb2be5ea04f589 = $this->env->getExtension("native_profiler");
        $__internal_3616bf6c58414df2b2d9a42e3f35945657c4c9e82bc0adfc6fdb2be5ea04f589->enter($__internal_3616bf6c58414df2b2d9a42e3f35945657c4c9e82bc0adfc6fdb2be5ea04f589_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_3616bf6c58414df2b2d9a42e3f35945657c4c9e82bc0adfc6fdb2be5ea04f589->leave($__internal_3616bf6c58414df2b2d9a42e3f35945657c4c9e82bc0adfc6fdb2be5ea04f589_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
    }
}
