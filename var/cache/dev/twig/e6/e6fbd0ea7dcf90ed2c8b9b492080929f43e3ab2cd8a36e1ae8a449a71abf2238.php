<?php

/* AppBundle::page.html.twig */
class __TwigTemplate_5064f63e10c7726a988b551aa157fd921d37d7b5481c7468e4ece237b2e61fae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "AppBundle::page.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3742255c13a6f2b911d89049ea47294fcaaab52d512729a1fc9bc6c04bf56571 = $this->env->getExtension("native_profiler");
        $__internal_3742255c13a6f2b911d89049ea47294fcaaab52d512729a1fc9bc6c04bf56571->enter($__internal_3742255c13a6f2b911d89049ea47294fcaaab52d512729a1fc9bc6c04bf56571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::page.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3742255c13a6f2b911d89049ea47294fcaaab52d512729a1fc9bc6c04bf56571->leave($__internal_3742255c13a6f2b911d89049ea47294fcaaab52d512729a1fc9bc6c04bf56571_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_02e7af5728ed13a207ac1caa784d4a0a4ced5f5d4cbae9ad43f6ff9ac337dd8a = $this->env->getExtension("native_profiler");
        $__internal_02e7af5728ed13a207ac1caa784d4a0a4ced5f5d4cbae9ad43f6ff9ac337dd8a->enter($__internal_02e7af5728ed13a207ac1caa784d4a0a4ced5f5d4cbae9ad43f6ff9ac337dd8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "title", array()), "html", null, true);
        
        $__internal_02e7af5728ed13a207ac1caa784d4a0a4ced5f5d4cbae9ad43f6ff9ac337dd8a->leave($__internal_02e7af5728ed13a207ac1caa784d4a0a4ced5f5d4cbae9ad43f6ff9ac337dd8a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6be6dc2a579ef0aba98a15834dc3bbacc42eec55cd32df0adbbca6c49ff8823b = $this->env->getExtension("native_profiler");
        $__internal_6be6dc2a579ef0aba98a15834dc3bbacc42eec55cd32df0adbbca6c49ff8823b->enter($__internal_6be6dc2a579ef0aba98a15834dc3bbacc42eec55cd32df0adbbca6c49ff8823b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo " <div class=\"col-md-12 col-xs-12 whitebox\">
  ";
        // line 5
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "body", array());
        echo "
 </div>

";
        
        $__internal_6be6dc2a579ef0aba98a15834dc3bbacc42eec55cd32df0adbbca6c49ff8823b->leave($__internal_6be6dc2a579ef0aba98a15834dc3bbacc42eec55cd32df0adbbca6c49ff8823b_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 5,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends 'base.html.twig' %}
{% block title %}{{  page.title }}{% endblock %}
{% block body %}
 <div class=\"col-md-12 col-xs-12 whitebox\">
  {{  page.body | raw }}
 </div>

{% endblock %}";
    }
}
