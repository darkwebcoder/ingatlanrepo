<?php

/* base.html.twig */
class __TwigTemplate_09c8d674f773279f4c980a99614669fc1dca299595fbe340980a4bbcd27f6db3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a69a234fccb3108d89d98ea1eab52642ae781a12786481f7d851dbddaacfaeb6 = $this->env->getExtension("native_profiler");
        $__internal_a69a234fccb3108d89d98ea1eab52642ae781a12786481f7d851dbddaacfaeb6->enter($__internal_a69a234fccb3108d89d98ea1eab52642ae781a12786481f7d851dbddaacfaeb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a9b6998_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_0") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/a9b6998_bootstrap_1.css");
            // line 11
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
        ";
            // asset "a9b6998_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_1") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/a9b6998_layout_2.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
        ";
            // asset "a9b6998_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_2") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/a9b6998_style_3.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "a9b6998"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998") : $this->env->getExtension('asset')->getAssetUrl("_controller/css/a9b6998.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 13
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    </head>
    <body>
    ";
        // line 17
        $this->loadTemplate("header.html.twig", "base.html.twig", 17)->display($context);
        // line 18
        echo "    <div class=\"container\">
        ";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "    </div>
    ";
        // line 21
        $this->loadTemplate("footer.html.twig", "base.html.twig", 21)->display($context);
        // line 22
        echo "    </body>
</html>
";
        
        $__internal_a69a234fccb3108d89d98ea1eab52642ae781a12786481f7d851dbddaacfaeb6->leave($__internal_a69a234fccb3108d89d98ea1eab52642ae781a12786481f7d851dbddaacfaeb6_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e47e6f7bdecd0491f7a021b6a50242e01c199f8a61134f775eaf8b196ae5d486 = $this->env->getExtension("native_profiler");
        $__internal_e47e6f7bdecd0491f7a021b6a50242e01c199f8a61134f775eaf8b196ae5d486->enter($__internal_e47e6f7bdecd0491f7a021b6a50242e01c199f8a61134f775eaf8b196ae5d486_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_e47e6f7bdecd0491f7a021b6a50242e01c199f8a61134f775eaf8b196ae5d486->leave($__internal_e47e6f7bdecd0491f7a021b6a50242e01c199f8a61134f775eaf8b196ae5d486_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_d0cb5306c2f8b16a96cc424bc1bf5783bb3e3e5d3df4a9558f1fa22d45f3a74a = $this->env->getExtension("native_profiler");
        $__internal_d0cb5306c2f8b16a96cc424bc1bf5783bb3e3e5d3df4a9558f1fa22d45f3a74a->enter($__internal_d0cb5306c2f8b16a96cc424bc1bf5783bb3e3e5d3df4a9558f1fa22d45f3a74a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_d0cb5306c2f8b16a96cc424bc1bf5783bb3e3e5d3df4a9558f1fa22d45f3a74a->leave($__internal_d0cb5306c2f8b16a96cc424bc1bf5783bb3e3e5d3df4a9558f1fa22d45f3a74a_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 19,  93 => 5,  84 => 22,  82 => 21,  79 => 20,  77 => 19,  74 => 18,  72 => 17,  64 => 13,  38 => 11,  34 => 6,  30 => 5,  24 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% stylesheets
        'assets/vendor/bootstrap/dist/css/bootstrap.css'
        'assets/vendor/templating/css/layout.css'
        'assets/vendor/templating/css/style.css'
            filter='cssrewrite' %}
            <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
        {% endstylesheets %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />

    </head>
    <body>
    {% include 'header.html.twig' %}
    <div class=\"container\">
        {% block body %}{% endblock %}
    </div>
    {% include 'footer.html.twig' %}
    </body>
</html>
";
    }
}
