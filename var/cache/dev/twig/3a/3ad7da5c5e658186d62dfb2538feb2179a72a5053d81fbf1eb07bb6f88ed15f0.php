<?php

/* CiberFlatBundle:Dashboard:index.html.twig */
class __TwigTemplate_0bb116cda2052e42402a46f3ba1e05ee59459a12caec6c55f32db2c69a798326 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CiberFlatBundle:Dashboard:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09934875f47ed02000dfa3b6ebe30b1a44c272b20198efa800760761544c6392 = $this->env->getExtension("native_profiler");
        $__internal_09934875f47ed02000dfa3b6ebe30b1a44c272b20198efa800760761544c6392->enter($__internal_09934875f47ed02000dfa3b6ebe30b1a44c272b20198efa800760761544c6392_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CiberFlatBundle:Dashboard:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09934875f47ed02000dfa3b6ebe30b1a44c272b20198efa800760761544c6392->leave($__internal_09934875f47ed02000dfa3b6ebe30b1a44c272b20198efa800760761544c6392_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_e99a44097c9911e9d177927d7ed62c94f36c4f00157c56a57b4a432bef1dbd76 = $this->env->getExtension("native_profiler");
        $__internal_e99a44097c9911e9d177927d7ed62c94f36c4f00157c56a57b4a432bef1dbd76->enter($__internal_e99a44097c9911e9d177927d7ed62c94f36c4f00157c56a57b4a432bef1dbd76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Your dashboard";
        
        $__internal_e99a44097c9911e9d177927d7ed62c94f36c4f00157c56a57b4a432bef1dbd76->leave($__internal_e99a44097c9911e9d177927d7ed62c94f36c4f00157c56a57b4a432bef1dbd76_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2bd860cae9bbceac0ab646656f75384fe7ae8d325210e2a9ede6fd8792d0fcb3 = $this->env->getExtension("native_profiler");
        $__internal_2bd860cae9bbceac0ab646656f75384fe7ae8d325210e2a9ede6fd8792d0fcb3->enter($__internal_2bd860cae9bbceac0ab646656f75384fe7ae8d325210e2a9ede6fd8792d0fcb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h2>
        Your dashboard
    </h2>

";
        
        $__internal_2bd860cae9bbceac0ab646656f75384fe7ae8d325210e2a9ede6fd8792d0fcb3->leave($__internal_2bd860cae9bbceac0ab646656f75384fe7ae8d325210e2a9ede6fd8792d0fcb3_prof);

    }

    public function getTemplateName()
    {
        return "CiberFlatBundle:Dashboard:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '::base.html.twig' %}
{% block title %}Your dashboard{% endblock %}
{% block body %}
    <h2>
        Your dashboard
    </h2>

{% endblock %}";
    }
}
