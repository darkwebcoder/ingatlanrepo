<?php

/* CiberFlatBundle:Default:flat-list-item.html.twig */
class __TwigTemplate_4513d03e85fa068104771d43392573347fc36e3c040af628aa12fc8d654f2387 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25b0eb98e5397dbbb5986c4deacf8440a1dbe7a7cca2c95ccb55f18e361222f7 = $this->env->getExtension("native_profiler");
        $__internal_25b0eb98e5397dbbb5986c4deacf8440a1dbe7a7cca2c95ccb55f18e361222f7->enter($__internal_25b0eb98e5397dbbb5986c4deacf8440a1dbe7a7cca2c95ccb55f18e361222f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CiberFlatBundle:Default:flat-list-item.html.twig"));

        // line 1
        echo "<div class=\"container flat-list-box\">
    <div class=\"col-md-4\">
        kép helye
    </div>
    <div class=\"col-md-8\">
        <div class=\"wrapper\">
            <figure class=\"right pad_left1\">
                <img src=\"images/page1_img2.jpg\" alt=\"\">
            </figure>
            <p class=\"font1\">Sed ut perspiciati natus error sit voluptatem accu- soloremque.</p>
        </div>
        <p class=\"font2 pad_bot2\">December 15, 2010</p>
        <p class=\"pad_bot2\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
        <a class=\"link1\" href=\"#\">Read More</a>
    </div>

</div>";
        
        $__internal_25b0eb98e5397dbbb5986c4deacf8440a1dbe7a7cca2c95ccb55f18e361222f7->leave($__internal_25b0eb98e5397dbbb5986c4deacf8440a1dbe7a7cca2c95ccb55f18e361222f7_prof);

    }

    public function getTemplateName()
    {
        return "CiberFlatBundle:Default:flat-list-item.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"container flat-list-box\">
    <div class=\"col-md-4\">
        kép helye
    </div>
    <div class=\"col-md-8\">
        <div class=\"wrapper\">
            <figure class=\"right pad_left1\">
                <img src=\"images/page1_img2.jpg\" alt=\"\">
            </figure>
            <p class=\"font1\">Sed ut perspiciati natus error sit voluptatem accu- soloremque.</p>
        </div>
        <p class=\"font2 pad_bot2\">December 15, 2010</p>
        <p class=\"pad_bot2\">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
        <a class=\"link1\" href=\"#\">Read More</a>
    </div>

</div>";
    }
}
