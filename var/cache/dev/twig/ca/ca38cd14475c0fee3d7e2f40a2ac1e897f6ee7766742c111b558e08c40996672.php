<?php

/* CiberFlatBundle:Default:index.html.twig */
class __TwigTemplate_74376591949b65859630eba86cde1f9638472881c9b26cb0e9692439280a91b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CiberFlatBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b641e2f1a7356e9aa755dce46600c284cef718841f2cdce98258939982b7a5f9 = $this->env->getExtension("native_profiler");
        $__internal_b641e2f1a7356e9aa755dce46600c284cef718841f2cdce98258939982b7a5f9->enter($__internal_b641e2f1a7356e9aa755dce46600c284cef718841f2cdce98258939982b7a5f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CiberFlatBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b641e2f1a7356e9aa755dce46600c284cef718841f2cdce98258939982b7a5f9->leave($__internal_b641e2f1a7356e9aa755dce46600c284cef718841f2cdce98258939982b7a5f9_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_47cf56dc66f1d588e292b65e115ab46e07cef9ef45035445a098cb20e1146216 = $this->env->getExtension("native_profiler");
        $__internal_47cf56dc66f1d588e292b65e115ab46e07cef9ef45035445a098cb20e1146216->enter($__internal_47cf56dc66f1d588e292b65e115ab46e07cef9ef45035445a098cb20e1146216_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Flats";
        
        $__internal_47cf56dc66f1d588e292b65e115ab46e07cef9ef45035445a098cb20e1146216->leave($__internal_47cf56dc66f1d588e292b65e115ab46e07cef9ef45035445a098cb20e1146216_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7866b56f5d1996eddef6d3916468e76b951061b00161248f6e169626f9ace301 = $this->env->getExtension("native_profiler");
        $__internal_7866b56f5d1996eddef6d3916468e76b951061b00161248f6e169626f9ace301->enter($__internal_7866b56f5d1996eddef6d3916468e76b951061b00161248f6e169626f9ace301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "   <h2>
      Flats for sale in Budapest
   </h2>
   ";
        // line 7
        echo twig_include($this->env, $context, "CiberFlatBundle:Default:flat-list-item.html.twig");
        echo "
";
        
        $__internal_7866b56f5d1996eddef6d3916468e76b951061b00161248f6e169626f9ace301->leave($__internal_7866b56f5d1996eddef6d3916468e76b951061b00161248f6e169626f9ace301_prof);

    }

    public function getTemplateName()
    {
        return "CiberFlatBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 7,  53 => 4,  47 => 3,  35 => 2,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '::base.html.twig' %}
{% block title %}Flats{% endblock %}
{% block body %}
   <h2>
      Flats for sale in Budapest
   </h2>
   {{ include('CiberFlatBundle:Default:flat-list-item.html.twig') }}
{% endblock %}
";
    }
}
