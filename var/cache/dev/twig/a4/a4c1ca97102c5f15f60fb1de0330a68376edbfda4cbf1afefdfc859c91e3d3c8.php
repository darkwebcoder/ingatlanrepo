<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_80904ef81446232fde6654daf1365c2f87de45a2ed7f6e8cb3d38b3ce395d8ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ddc4c9e517ad588652f05ebe6d8779be11462cb7b4179eaa3305fa006bf37b8 = $this->env->getExtension("native_profiler");
        $__internal_6ddc4c9e517ad588652f05ebe6d8779be11462cb7b4179eaa3305fa006bf37b8->enter($__internal_6ddc4c9e517ad588652f05ebe6d8779be11462cb7b4179eaa3305fa006bf37b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ddc4c9e517ad588652f05ebe6d8779be11462cb7b4179eaa3305fa006bf37b8->leave($__internal_6ddc4c9e517ad588652f05ebe6d8779be11462cb7b4179eaa3305fa006bf37b8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_03bfb727cb5fe1b7f760ae33e0b25712a03e8b2fe275b72035b332b1818813f1 = $this->env->getExtension("native_profiler");
        $__internal_03bfb727cb5fe1b7f760ae33e0b25712a03e8b2fe275b72035b332b1818813f1->enter($__internal_03bfb727cb5fe1b7f760ae33e0b25712a03e8b2fe275b72035b332b1818813f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_03bfb727cb5fe1b7f760ae33e0b25712a03e8b2fe275b72035b332b1818813f1->leave($__internal_03bfb727cb5fe1b7f760ae33e0b25712a03e8b2fe275b72035b332b1818813f1_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_dcb67b17be0844d4850753022e8b966f6ab6f51d8ba636996be09ca43c2c916c = $this->env->getExtension("native_profiler");
        $__internal_dcb67b17be0844d4850753022e8b966f6ab6f51d8ba636996be09ca43c2c916c->enter($__internal_dcb67b17be0844d4850753022e8b966f6ab6f51d8ba636996be09ca43c2c916c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_dcb67b17be0844d4850753022e8b966f6ab6f51d8ba636996be09ca43c2c916c->leave($__internal_dcb67b17be0844d4850753022e8b966f6ab6f51d8ba636996be09ca43c2c916c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e1a1146cb5b089a15fde734c38c645ab831b634a0e8e9982aabbd6e5f98e2429 = $this->env->getExtension("native_profiler");
        $__internal_e1a1146cb5b089a15fde734c38c645ab831b634a0e8e9982aabbd6e5f98e2429->enter($__internal_e1a1146cb5b089a15fde734c38c645ab831b634a0e8e9982aabbd6e5f98e2429_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_e1a1146cb5b089a15fde734c38c645ab831b634a0e8e9982aabbd6e5f98e2429->leave($__internal_e1a1146cb5b089a15fde734c38c645ab831b634a0e8e9982aabbd6e5f98e2429_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
";
    }
}
