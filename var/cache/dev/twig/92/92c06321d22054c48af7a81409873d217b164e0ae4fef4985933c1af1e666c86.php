<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_ee54dacb2bc900d17fc96cdea821274f6c62cf761c53e9f00012d6fcfcb37bf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0764e5543e9b755f6fef1820c846dd9f80350a444275ca52c4c2739cded31036 = $this->env->getExtension("native_profiler");
        $__internal_0764e5543e9b755f6fef1820c846dd9f80350a444275ca52c4c2739cded31036->enter($__internal_0764e5543e9b755f6fef1820c846dd9f80350a444275ca52c4c2739cded31036_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0764e5543e9b755f6fef1820c846dd9f80350a444275ca52c4c2739cded31036->leave($__internal_0764e5543e9b755f6fef1820c846dd9f80350a444275ca52c4c2739cded31036_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c830928f224cb9438e185ed09a8c9a73ffea974f146ca35fb392ce3dd938c2ad = $this->env->getExtension("native_profiler");
        $__internal_c830928f224cb9438e185ed09a8c9a73ffea974f146ca35fb392ce3dd938c2ad->enter($__internal_c830928f224cb9438e185ed09a8c9a73ffea974f146ca35fb392ce3dd938c2ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c830928f224cb9438e185ed09a8c9a73ffea974f146ca35fb392ce3dd938c2ad->leave($__internal_c830928f224cb9438e185ed09a8c9a73ffea974f146ca35fb392ce3dd938c2ad_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_be4c7a58eb27c1a4bf27dd79a42463723bf992f07860b22e41dd0c64d1dc3253 = $this->env->getExtension("native_profiler");
        $__internal_be4c7a58eb27c1a4bf27dd79a42463723bf992f07860b22e41dd0c64d1dc3253->enter($__internal_be4c7a58eb27c1a4bf27dd79a42463723bf992f07860b22e41dd0c64d1dc3253_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_be4c7a58eb27c1a4bf27dd79a42463723bf992f07860b22e41dd0c64d1dc3253->leave($__internal_be4c7a58eb27c1a4bf27dd79a42463723bf992f07860b22e41dd0c64d1dc3253_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_83ad3260bc6089167e3a43bdfa7a4f6afa15c8286ad60cf946435f9af14b21ae = $this->env->getExtension("native_profiler");
        $__internal_83ad3260bc6089167e3a43bdfa7a4f6afa15c8286ad60cf946435f9af14b21ae->enter($__internal_83ad3260bc6089167e3a43bdfa7a4f6afa15c8286ad60cf946435f9af14b21ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_83ad3260bc6089167e3a43bdfa7a4f6afa15c8286ad60cf946435f9af14b21ae->leave($__internal_83ad3260bc6089167e3a43bdfa7a4f6afa15c8286ad60cf946435f9af14b21ae_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
";
    }
}
