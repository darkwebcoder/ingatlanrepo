<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_22a4f6f050f266230f6530ee33523137cf7d4fe89652b90bed89ba461217d15e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d7d5f7fdf897d8af28be74efaae997ba185b0e7709e4a7142766d1d6a7f5b37 = $this->env->getExtension("native_profiler");
        $__internal_4d7d5f7fdf897d8af28be74efaae997ba185b0e7709e4a7142766d1d6a7f5b37->enter($__internal_4d7d5f7fdf897d8af28be74efaae997ba185b0e7709e4a7142766d1d6a7f5b37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d7d5f7fdf897d8af28be74efaae997ba185b0e7709e4a7142766d1d6a7f5b37->leave($__internal_4d7d5f7fdf897d8af28be74efaae997ba185b0e7709e4a7142766d1d6a7f5b37_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f217546a4cd76216fd5ce64a764712adbba752e1c6725819b37f269dab18c932 = $this->env->getExtension("native_profiler");
        $__internal_f217546a4cd76216fd5ce64a764712adbba752e1c6725819b37f269dab18c932->enter($__internal_f217546a4cd76216fd5ce64a764712adbba752e1c6725819b37f269dab18c932_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f217546a4cd76216fd5ce64a764712adbba752e1c6725819b37f269dab18c932->leave($__internal_f217546a4cd76216fd5ce64a764712adbba752e1c6725819b37f269dab18c932_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e1f9fb182bf74f500e695b5db3e3feaf9082c66ee38c9b27b3c895b9b98f46c3 = $this->env->getExtension("native_profiler");
        $__internal_e1f9fb182bf74f500e695b5db3e3feaf9082c66ee38c9b27b3c895b9b98f46c3->enter($__internal_e1f9fb182bf74f500e695b5db3e3feaf9082c66ee38c9b27b3c895b9b98f46c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e1f9fb182bf74f500e695b5db3e3feaf9082c66ee38c9b27b3c895b9b98f46c3->leave($__internal_e1f9fb182bf74f500e695b5db3e3feaf9082c66ee38c9b27b3c895b9b98f46c3_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_ab238f90e405e8e161aa345b40f1382de7996fe4a0564c716b9b0650da05cf63 = $this->env->getExtension("native_profiler");
        $__internal_ab238f90e405e8e161aa345b40f1382de7996fe4a0564c716b9b0650da05cf63->enter($__internal_ab238f90e405e8e161aa345b40f1382de7996fe4a0564c716b9b0650da05cf63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_ab238f90e405e8e161aa345b40f1382de7996fe4a0564c716b9b0650da05cf63->leave($__internal_ab238f90e405e8e161aa345b40f1382de7996fe4a0564c716b9b0650da05cf63_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
";
    }
}
