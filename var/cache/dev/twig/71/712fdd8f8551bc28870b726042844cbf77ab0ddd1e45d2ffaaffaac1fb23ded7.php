<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_3db165a6cda183583a1b010031136eaed144a003329374721980d757dc3104ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa41de4fd2a126e306c7c8fcb46ca5152faddc3c0db2ca475ea152283e876687 = $this->env->getExtension("native_profiler");
        $__internal_aa41de4fd2a126e306c7c8fcb46ca5152faddc3c0db2ca475ea152283e876687->enter($__internal_aa41de4fd2a126e306c7c8fcb46ca5152faddc3c0db2ca475ea152283e876687_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_aa41de4fd2a126e306c7c8fcb46ca5152faddc3c0db2ca475ea152283e876687->leave($__internal_aa41de4fd2a126e306c7c8fcb46ca5152faddc3c0db2ca475ea152283e876687_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }

    public function getSource()
    {
        return "/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
";
    }
}
