<?php

/* footer.html.twig */
class __TwigTemplate_475d80c6fef006f90760b9685d37e9886ba75ae8a57b748706b90d66265ed12f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"body3\">
    <div class=\"body4\">
        <div class=\"main\">
            <section id=\"content2\">
                <article class=\"col3 pad_left1\">
                    <h3>Buying</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img3.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">16.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Praesent vestibulum molestie lacenean nonummy hendrerit mauri asefporaum sociis penatibus natoque.</p>
                    <a href=\"#\" class=\"line_right\">6 Comments</a> <a href=\"#\">Read More</a>
                </article>
                <article class=\"col3 pad_left2\">
                    <h3>Selling</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img4.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">11.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    <a href=\"#\" class=\"line_right\">2 Comments</a> <a href=\"#\">Read More</a>
                </article>
                <article class=\"col3 pad_left2\">
                    <h3>Renting</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img5.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">13.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Nemo enim ipsam voluptatem voluptas sit aspernatur aut odit aut fugit, sedquia penatibus magnis.</p>
                    <a href=\"#\" class=\"line_right\">9 Comments</a> <a href=\"#\">Read More</a>
                </article>
            </section>
        </div>
    </div>
</div>
    <!--content end-->
    <!--footer -->
    <footer>
        2016 - ingatlanpiac.eu
    </footer>
    <!--footer end-->
<script type=\"text/javascript\"> Cufon.now(); </script>
        ";
        // line 40
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f93252f_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f93252f_0") : $this->env->getExtension('asset')->getAssetUrl("js/f93252f_jquery_1.js");
            // line 45
            echo "<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "f93252f_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f93252f_1") : $this->env->getExtension('asset')->getAssetUrl("js/f93252f_bootstrap_2.js");
            echo "<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "f93252f_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f93252f_2") : $this->env->getExtension('asset')->getAssetUrl("js/f93252f_ckeditor_3.js");
            echo "<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "f93252f"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f93252f") : $this->env->getExtension('asset')->getAssetUrl("js/f93252f.js");
            echo "<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 45,  60 => 40,  19 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"body3\">
    <div class=\"body4\">
        <div class=\"main\">
            <section id=\"content2\">
                <article class=\"col3 pad_left1\">
                    <h3>Buying</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img3.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">16.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Praesent vestibulum molestie lacenean nonummy hendrerit mauri asefporaum sociis penatibus natoque.</p>
                    <a href=\"#\" class=\"line_right\">6 Comments</a> <a href=\"#\">Read More</a>
                </article>
                <article class=\"col3 pad_left2\">
                    <h3>Selling</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img4.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">11.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
                    <a href=\"#\" class=\"line_right\">2 Comments</a> <a href=\"#\">Read More</a>
                </article>
                <article class=\"col3 pad_left2\">
                    <h3>Renting</h3>
                    <figure class=\"pad_bot1\"><img src=\"images/page1_img5.jpg\" alt=\"\"></figure>
                    <p class=\"pad_bot2\"><a href=\"#\">13.12.2010</a></p>
                    <p class=\"pad_bot2\">
                        Nemo enim ipsam voluptatem voluptas sit aspernatur aut odit aut fugit, sedquia penatibus magnis.</p>
                    <a href=\"#\" class=\"line_right\">9 Comments</a> <a href=\"#\">Read More</a>
                </article>
            </section>
        </div>
    </div>
</div>
    <!--content end-->
    <!--footer -->
    <footer>
        2016 - ingatlanpiac.eu
    </footer>
    <!--footer end-->
<script type=\"text/javascript\"> Cufon.now(); </script>
        {% javascripts
        'assets/vendor/jquery/dist/jquery.js'
        'assets/vendor/bootstrap/dist/js/bootstrap.js'
        'assets/vendor/ckeditor/ckeditor.js'
        %}
<script src=\"{{ asset_url }}\"></script>
{% endjavascripts %}
";
    }
}
