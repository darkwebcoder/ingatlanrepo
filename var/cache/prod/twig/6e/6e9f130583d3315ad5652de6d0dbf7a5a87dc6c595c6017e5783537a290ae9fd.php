<?php

/* base.html.twig */
class __TwigTemplate_844bd0acc519194a5810f04c523e8740a8762427885ee0e00b4724caa0307668 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a9b6998_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_0") : $this->env->getExtension('asset')->getAssetUrl("css/a9b6998_bootstrap_1.css");
            // line 11
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
            // asset "a9b6998_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_1") : $this->env->getExtension('asset')->getAssetUrl("css/a9b6998_layout_2.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
            // asset "a9b6998_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998_2") : $this->env->getExtension('asset')->getAssetUrl("css/a9b6998_style_3.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "a9b6998"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a9b6998") : $this->env->getExtension('asset')->getAssetUrl("css/a9b6998.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 13
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />

    </head>
    <body>
    ";
        // line 17
        $this->loadTemplate("header.html.twig", "base.html.twig", 17)->display($context);
        // line 18
        echo "    <div class=\"container\">
        ";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "    </div>
    ";
        // line 21
        $this->loadTemplate("footer.html.twig", "base.html.twig", 21)->display($context);
        // line 22
        echo "    </body>
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 19,  87 => 5,  81 => 22,  79 => 21,  76 => 20,  74 => 19,  71 => 18,  69 => 17,  61 => 13,  35 => 11,  31 => 6,  27 => 5,  21 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% stylesheets
        'assets/vendor/bootstrap/dist/css/bootstrap.css'
        'assets/vendor/templating/css/layout.css'
        'assets/vendor/templating/css/style.css'
            filter='cssrewrite' %}
            <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
        {% endstylesheets %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />

    </head>
    <body>
    {% include 'header.html.twig' %}
    <div class=\"container\">
        {% block body %}{% endblock %}
    </div>
    {% include 'footer.html.twig' %}
    </body>
</html>
";
    }
}
