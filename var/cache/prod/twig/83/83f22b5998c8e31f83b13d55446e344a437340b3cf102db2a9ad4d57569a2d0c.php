<?php

/* header.html.twig */
class __TwigTemplate_3f9eb6bafa369abfa0fc5a0125aa51a334710c3bae19e1c858210bc62a7a38b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--header -->
<header>
    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a href=\"index.html\" id=\"logo\">Estate</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a class=\"btn btn-menu\" href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getUrl("homepage");
        echo "\">Home</a></li>
                        <li><a class=\"btn btn-menu\" href=\"/flats\">Flats</a></li>
                        <li><a class=\"btn btn-menu\" href=\"#\">About</a></li>
                        <li><a class=\"btn btn-menu\" href=\"#\">Contact</a></li>

                    </ul>
                    <ul class=\"nav navbar-nav navbar-right\">
                        ";
        // line 24
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 25
            echo "                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"btn-alter btn-menu dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"> <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo "  <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></span> Dashboard</a></li>
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon-envelope\" aria-hidden=\"true\"></span> Income messages</a></li>
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> Profile settings</a></li>
                                    ";
            // line 31
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 32
                echo "                                        <li><a href=\"/app_dev.php/admin\"><span class=\"glyphicon glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> Adminisztráció</a></li>

                                    ";
            }
            // line 35
            echo "                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a  href=\"";
            // line 36
            echo $this->env->getExtension('routing')->getUrl("fos_user_security_logout");
            echo "\">Log out</a></li>
                                </ul>
                            </li>
                        ";
        } else {
            // line 40
            echo "                            <li><a class=\"btn btn-menu\" href=\"";
            echo $this->env->getExtension('routing')->getUrl("fos_user_security_login");
            echo "\"><span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> Log in</a></li>
                        ";
        }
        // line 42
        echo "

                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

    </div>
</header>
<hr>
<!--header end-->";
    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 42,  77 => 40,  70 => 36,  67 => 35,  62 => 32,  60 => 31,  52 => 26,  49 => 25,  47 => 24,  37 => 17,  19 => 1,);
    }

    public function getSource()
    {
        return "<!--header -->
<header>
    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a href=\"index.html\" id=\"logo\">Estate</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a class=\"btn btn-menu\" href=\"{{ url('homepage') }}\">Home</a></li>
                        <li><a class=\"btn btn-menu\" href=\"/flats\">Flats</a></li>
                        <li><a class=\"btn btn-menu\" href=\"#\">About</a></li>
                        <li><a class=\"btn btn-menu\" href=\"#\">Contact</a></li>

                    </ul>
                    <ul class=\"nav navbar-nav navbar-right\">
                        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"btn-alter btn-menu dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"> <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> {{ app.user.username }}  <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=\"true\"></span> Dashboard</a></li>
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon-envelope\" aria-hidden=\"true\"></span> Income messages</a></li>
                                    <li><a href=\"#\"><span class=\"glyphicon glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> Profile settings</a></li>
                                    {% if is_granted('ROLE_ADMIN') %}
                                        <li><a href=\"/app_dev.php/admin\"><span class=\"glyphicon glyphicon glyphicon-cog\" aria-hidden=\"true\"></span> Adminisztráció</a></li>

                                    {% endif %}
                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a  href=\"{{ url('fos_user_security_logout') }}\">Log out</a></li>
                                </ul>
                            </li>
                        {% else %}
                            <li><a class=\"btn btn-menu\" href=\"{{ url('fos_user_security_login') }}\"><span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span> Log in</a></li>
                        {% endif %}


                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

    </div>
</header>
<hr>
<!--header end-->";
    }
}
