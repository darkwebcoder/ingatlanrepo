jQuery(document).ready(function() {

    $('ul.prices').SymfonyMultiForm();

    $('ul.flatimages').SymfonyMultiForm({
        addLinkText: "Add new pictures",
        addLinkIcon: "glyphicon glyphicon-plus",
        addLinkClass: "btn btn-success",
        removeLinkText: "Remove picture",
        removeLinkIcon: "glyphicon glyphicon-minus",
        removeLinkClass: "btn btn-danger",
        allow_delete: true
    });

});

