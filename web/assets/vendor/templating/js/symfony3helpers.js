/**
 * Created by Kreets on 2016.10.21..
 */
(function ( $ ) {

    $.fn.SymfonyMultiForm = function(options ) {
        var settings = $.extend({
            // These are the defaults.
            addLinkText: "Add",
            addLinkIcon: "",
            addLinkClass: "",
            removeLinkText: "Remove",
            removeLinkIcon: "",
            removeLinkClass: "",
            allow_delete: true
        }, options );

        $element = this;
        // Add links
        var glyphicon = "";
        if(settings.addLinkIcon != ""){
            glyphicon = '<span class="'+settings.addLinkIcon+'" aria-hidden="true"></span> ';
        }
        var removeLink = "";
        var removeLinkHtml = "";
        if(settings.allow_delete){
            var glyphiconremove = "";
            if(settings.removeLinkIcon != ""){
                glyphiconremove = '<span class="'+settings.removeLinkIcon+'" aria-hidden="true"></span> ';
            }
            removeLink = $('<a href="#" class="remove_link '+settings.removeLinkClass+'">'+glyphiconremove+settings.removeLinkText+'</a>');
            $element.find('li').each(function() {
                $(this).append(removeLink);
            });
            var removeLinkHtml = $("<div />").append(removeLink.clone()).html();
        }

        $addLink = $('<a href="#" class="add_link '+settings.addLinkClass+'" data-prototype="'+htmlEntities(removeLinkHtml)+'">'+glyphicon+settings.addLinkText+'</a>');
        $newLinkLi = $('<li></li>').append($addLink);

        $element.append($newLinkLi)
        $element.data('index', $element.find(':input').length);

        $addLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // add a new form (see next code block)
            $.fn.SymfonyMultiForm.addForm($(this), settings.removeLinkText, settings.removeLinkClass, settings.removeLinkIcon);
        });
        return this;
    };
    $.fn.SymfonyMultiForm.addForm = function($button)
    {
            $element = $button.parents('ul');
            $newLinkLi = $button.parent('li');
            $removeButton = $($button.attr('data-prototype'));
            // Get the data-prototype explained earlier
            var prototype = $element.data('prototype');
            // get the new index
            var index = $element.data('index');
            // Replace '__name__' in the prototype's HTML to
            // instead be a number based on how many items we have
            newForm = prototype.replace(/__name__/g, index);
            // increase the index with one for the next item
            $element.data('index', index + 1);
            // Display the form in the page in an li, before the "Add" link li
            $newFormLi = $('<li></li>').append(newForm);
            $.fn.SymfonyMultiForm.addRemoveButton($newFormLi, $removeButton)
            $newLinkLi.before($newFormLi);
    }

    $.fn.SymfonyMultiForm.addRemoveButton = function($li, $removeLink){
        // Remove links$removeLink = $('<a href="#" class="remove_link '+settings.removeLinkClass+'">'+glyphicon+settings.removeLinkText+'</a>');
        $li.append($removeLink);
        $removeLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            // remove the li for the tag form
            $li.remove();
        });

    }
}( jQuery ));
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}