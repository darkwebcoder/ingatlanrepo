<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Exception;

class PageController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }
    /**
     * @Route("/{page]", name="homepage")
     */
    public function pageAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository('AppBundle:Page')->findOneBy(array('slug' => (is_null($request->get('page')) ? 'homepage' : $request->get('page') )));
        if(!$page){
            throw new Exception('Nincs ilyen aloldal');
        }

        // replace this example code with whatever you need
        return $this->render('AppBundle::page.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'page' => $page
        ]);
    }
}
