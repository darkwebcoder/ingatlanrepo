<?php
namespace Ciber\FlatBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FlatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('address', AddressType::class)
            ->add('contact', ContactType::class)
            ->add('is_rentable', CheckboxType::class, array(
                'label'    => 'Available for renting',
                'required' => false,
            ))
            ->add('is_buyable', CheckboxType::class, array(
                'label'    => 'Available for sell',
                'required' => false,
            ))
            ->add('prices', CollectionType::class, array(
                'entry_type' => PriceType::class,
                'allow_add'    => true,
                'allow_delete' => true,
            ))
            ->add('flatImages', CollectionType::class, array(
                'entry_type' => FlatImageType::class,
                'allow_add'    => true,
                'allow_delete' => true,
            ));

        $builder->add('save', SubmitType::class);
    }



    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ciber\FlatBundle\Entity\Flat',
        ));
    }
}