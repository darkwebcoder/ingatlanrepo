<?php
/**
 * Created by PhpStorm.
 * User: Kreets
 * Date: 2016.10.19.
 * Time: 21:31
 */

namespace Ciber\FlatBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('amount');
        $builder->add('currency', CurrencyType::class, array(
            'placeholder' => 'Select currency',
        ));
        $builder->add('price_type', EntityType::class, array(
            // query choices from this entity
            'class' => 'Ciber\FlatBundle\Entity\PriceType'

            // used to render a select box, check boxes or radios
            // 'multiple' => true,
            // 'expanded' => true,
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ciber\FlatBundle\Entity\Price',
        ));
    }
}