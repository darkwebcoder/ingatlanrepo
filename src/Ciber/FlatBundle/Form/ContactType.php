<?php
/**
 * Created by PhpStorm.
 * User: Kreets
 * Date: 2016.10.19.
 * Time: 21:31
 */

namespace Ciber\FlatBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact_name');
        $builder->add('contact_phone');
        $builder->add('contact_email');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ciber\FlatBundle\Entity\Contact',
        ));
    }
}