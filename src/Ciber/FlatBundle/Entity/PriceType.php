<?php

namespace Ciber\FlatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PriceType
 *
 * @ORM\Table(name="price_type")
 * @ORM\Entity(repositoryClass="Ciber\FlatBundle\Repository\PriceTypeRepository")
 */
class PriceType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Ciber\FlatBundle\Entity\Price", mappedBy="priceType")
     */
    private $prices;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PriceType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
}

