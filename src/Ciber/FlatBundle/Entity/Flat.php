<?php

namespace Ciber\FlatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Flat
 *
 * @ORM\Table(name="flat")
 * @ORM\Entity(repositoryClass="Ciber\FlatBundle\Repository\FlatRepository")
 */
class Flat
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="Ciber\FlatBundle\Entity\Address", mappedBy="flat", cascade={"persist"})
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="Ciber\FlatBundle\Entity\Contact", inversedBy="flats", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity="Ciber\FlatBundle\Entity\Price", mappedBy="flat", cascade={"persist", "remove"})
     */
    protected $prices;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", options={"default":0} )
     */
    private $isActive;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ciber\FlatBundle\Entity\FlatImage", mappedBy="flat", cascade={"persist", "remove"})
     */
    private $flatImages;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_rentable", type="boolean", nullable=true)
     */
    private $isRentable;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_buyable", type="boolean", nullable=true)
     */
    private $isBuyable;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->flatImages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->prices = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Flat
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Flat
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set adresss
     *
     * @param integer $adresss
     *
     * @return Flat
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get adresss
     *
     * @return int
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contactId
     *
     * @param integer $contact
     *
     * @return Flat
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return int
     */
    public function getContact()
    {
        return $this->contact;
    }

    public function getPrices()
    {
        return $this->prices;
    }

    public function setPrices($prices)
    {
        $this->prices[] = $prices;
    }

    /**
     * Add flatImages
     *
     * @param \Ciber\FlatBundle\Entity\FlatImage $flatImages
     * @return Flat
     */
    public function addFlatImage(\Ciber\FlatBundle\Entity\FlatImage $flatImages)
    {
        $this->flatImages[] = $flatImages;

        return $this;
    }

    /**
     * Remove flatImages
     *
     * @param \Ciber\FlatBundle\Entity\FlatImage $flatImages
     */
    public function removeFlatImage(\Ciber\FlatBundle\Entity\FlatImage $flatImages)
    {
        $this->flatImages->removeElement($flatImages);
    }

    /**
     * Get flatImages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFlatImages()
    {
        return $this->flatImages;
    }
    
    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Flat
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Flat
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set isRentable
     *
     * @param boolean $isRentable
     *
     * @return Flat
     */
    public function setIsRentable($isRentable)
    {
        $this->isRentable = $isRentable;

        return $this;
    }

    /**
     * Get isRentable
     *
     * @return bool
     */
    public function getIsRentable()
    {
        return $this->isRentable;
    }

    /**
     * Set isBuyable
     *
     * @param boolean $isBuyable
     *
     * @return Flat
     */
    public function setIsBuyable($isBuyable)
    {
        $this->isBuyable = $isBuyable;

        return $this;
    }

    /**
     * Get isBuyable
     *
     * @return bool
     */
    public function getIsBuyable()
    {
        return $this->isBuyable;
    }

    public function getAddressId(){
        return $this->addressId;
    }

    public function addPrice($price){
        $this->prices[] = $price;
    }

    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }
}

