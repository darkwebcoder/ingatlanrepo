<?php

namespace Ciber\FlatBundle\Controller;

use Ciber\FlatBundle\Entity\Flat;

use Ciber\FlatBundle\Form\FlatType;


use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Exception;

class DashboardController extends Controller
{

    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction(Request $request)
    {
        return $this->render('CiberFlatBundle:Dashboard:index.html.twig');
    }

    /**
     * @Route("/flat/new", name="add_flat")
     */
    public function addFlatAction(Request $request)
    {

        $form = $this->createForm(FlatType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()){
                // $form->getData() holds the submitted values
                // but, the original `$task` variable has also been updated
                $flat = $form->getData();
                $flat->setUserId($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($flat);
                $em->flush();
            }

        }

        return $this->render('CiberFlatBundle:Dashboard:newflat.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
